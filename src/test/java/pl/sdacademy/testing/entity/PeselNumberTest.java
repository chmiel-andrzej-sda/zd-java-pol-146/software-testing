package pl.sdacademy.testing.entity;

import org.assertj.core.api.ThrowableAssert;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.ValueSource;

import static org.assertj.core.api.Assertions.assertThat;
import static org.assertj.core.api.Assertions.assertThatThrownBy;

class PeselNumberTest {
	@BeforeEach
	public void beforeEach() {
		System.out.println("Before Each");
	}

	@BeforeAll
	public static void beforeAll() {
		System.out.println("Before All");
	}

	@AfterAll
	public static void afterAll() {
		System.out.println("After All");
	}

	@AfterEach
	public void afterEach() {
		System.out.println("After Each");
	}

	@ParameterizedTest
	@ValueSource(strings = {"12345678901", "12341234123", "22222222222"})
	void testCorrectPesel(final String value) {
		// when
		final PeselNumber peselNumber = new PeselNumber(value);

		// then
		assertThat(peselNumber.value()).isEqualTo(value);
	}

	@ParameterizedTest
	@ValueSource(strings = {"1234", "", "123412341234"})
	void testIncorrectPesel(final String value) {
		final ThrowableAssert.ThrowingCallable callable = () -> new PeselNumber(value);

		// then
		assertThatThrownBy(callable)
				.isInstanceOf(IllegalArgumentException.class)
				.hasMessage("Pesel number must contain exactly 11 chars!");
	}
}

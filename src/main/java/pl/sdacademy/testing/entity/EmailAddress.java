package pl.sdacademy.testing.entity;

public record EmailAddress(String value) {
	public EmailAddress {
		if (!value.contains("@")) {
			throw new IllegalArgumentException("Email address must contain '@' sign!");
		}
	}
}
